﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example3
{
    class Program
    {
        static void Main(string[] args)
        {
            var car = new CollectionCar();
            var it = car.CreateIterator();

            while (it.HasNext())
            {
                Console.WriteLine(it.GetNext().Number);
            }

            Console.ReadKey();
        }
    }

    public class Car
    {
        public string Number { get; set; }
    }

    public interface Iterator
    {
        bool HasNext();
        Car GetNext();
        Car First();
    }

    public class ConcreteInterator : Iterator
    {
        INumerable numerable;
        int _current = 0;

        public ConcreteInterator(INumerable numerable)
        {
            this.numerable = numerable;
        }

        public Car First()
        {
            return numerable[0];
        }

        public Car GetNext()
        {
            var a = numerable[_current];
            _current++;
            return a;
        }

        public bool HasNext()
        {
            return numerable.Count() > _current;
        }
    }

    public interface INumerable
    {
        Iterator CreateIterator();
        int Count();
        Car this[int index] { get; }
    }

    public class CollectionCar : INumerable
    {
        public List<Car> Car { get; set; } = new List<Car>() { new Car { Number = "1" }, new Car { Number = "2" }, new Car { Number = "3" }, new Car { Number = "4" } };

        public Car this[int index] { get => Car.ElementAt(index); }

        public int Count()
        {
            return Car.Count;
        }

        public Iterator CreateIterator()
        {
            return new ConcreteInterator(this);
        }
    }
}
