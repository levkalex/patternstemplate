﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento__Снимок____Повед
{
    public class Hero
    {
        int Life { get; set; }
        int Amunition { get; set; }

        public Hero()
        {
            Life = 10;
            Amunition = 10;
        }

        public void Shot()
        {
            Amunition--;

            if (new Random().Next(0, 10) > 4) Life--;
        }

        public void Info()
        {
            Console.WriteLine($"Life: {Life} Amunition: {Amunition}");
        }

        public IMemento Save()
        {
            return new HeroMemento(this.Life, this.Amunition, "Memento");
        }

        // Восстанавливает состояние Создателя из объекта снимка.
        public void Restore(IMemento memento)
        {
            if (!(memento is HeroMemento))
            {
                throw new Exception("Unknown memento class " + memento.Name);
            }

            Life = ((HeroMemento)memento).Life;
            Amunition = ((HeroMemento)memento).Amunition;
        }
    }

    public interface IMemento
    {
         DateTime DateTime { get; set; }
         string Name { get; set; }
    }

    class HeroMemento : IMemento
    {
        public int Life { get; set; }
        public int Amunition { get; set; }
        public DateTime DateTime { get; set; }
        public string Name { get; set; }

        public HeroMemento(int Life, int Amunition, string name = "")
        {
            this.Life = Life;
            this.Amunition = Amunition;
            this.Name = name;
            DateTime = DateTime.Now;
        }
    }

    public class Caretaker
    {
        List<IMemento> Mementos { get; set; }

        public Hero Hero { get; set; }

        public Caretaker()
        {
            Mementos = new List<IMemento>();
            Hero = new Hero();
        }

        public void Save()
        {
            Mementos.Add(Hero.Save());
        }

        public void RestoreLast()
        {
            Hero.Restore(Mementos.Last());
            Mementos.Remove(Mementos.Last());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var caretaker = new Caretaker();

            caretaker.Hero.Shot();
            caretaker.Hero.Shot();

            caretaker.Hero.Info();
            caretaker.Save();

            caretaker.Hero.Shot();
            caretaker.Hero.Shot();
            caretaker.Hero.Shot();

            caretaker.Hero.Info();
            caretaker.Save();

            caretaker.Hero.Shot();
            caretaker.Hero.Shot();
            caretaker.Hero.Shot();
            caretaker.Hero.Shot();

            caretaker.Hero.Info();

            caretaker.RestoreLast();
            caretaker.Hero.Info();
            caretaker.RestoreLast();
            caretaker.Hero.Info();
            Console.ReadKey();
        }
    }
}
