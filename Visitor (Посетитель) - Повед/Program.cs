﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor__Посетитель____Повед
{
    public class Building
    {
        public string Adress { get; set; }
        public double Rating { get; set; }
    }

    public class Museium : Building, IImport
    {
        public int CountPictures { get; set; }

        public void accept(IImportXml import)
        {
            import.ExportMuseium(this);
        }
    }

    public class Cafe : Building, IImport
    {
        public int Capacity { get; set; }
        public double AveragePrice { get; set; }

        public void accept(IImportXml import)
        {
            import.ExportCafe(this);
        }
    }

    public class House : Building, IImport
    {
        public void accept(IImportXml import)
        {
            import.ExportHouse(this);
        }
    }

    public interface IImport
    {
        void accept(IImportXml import);
    }

    //Need make import in xml all object
    public interface IImportXml
    {
        void ExportMuseium(Museium museium);
        void ExportCafe(Cafe cafe);
        void ExportHouse(House house);
    }

    public class ImportXml : IImportXml
    {
        public void ExportCafe(Cafe cafe)
        {
            Console.WriteLine("Export object cafe: " + cafe.Adress);
        }

        public void ExportHouse(House house)
        {
            Console.WriteLine("Export object house: " + house.Adress);
        }

        public void ExportMuseium(Museium museium)
        {
            Console.WriteLine("Export object museium: " + museium.Adress);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IImportXml import = new ImportXml();

            IImport cafe = new Cafe() { Adress = "Sviridova 80" };
            IImport museium = new Museium() { Adress = "Sovetskaya 20" };
            IImport house = new House() { Adress = "Kalvariiskaya 22" };

            cafe.accept(import);
            museium.accept(import);
            house.accept(import);

            Console.ReadKey();
        }
    }
}
