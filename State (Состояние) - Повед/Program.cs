﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace State__Состояние____Повед
{
    public class Gun
    {
        State state { get; set; }
        public int Ammunition { get; set; }
        public Gun(int ammunition)
        {
            Ammunition = ammunition;
            state = new ReadyState(this);
        }

        public void SetState(State state)
        {
            this.state = state;
        }

        public void Shot()
        {
            state.Shot();
        }

        public void Fuse() //предохранитель
        {
            state.Fuse();
        }

        public void Recharge()
        {
            state.Recharge();
        }
    }

    public abstract class BaseState
    {
        protected Gun gun;
        public BaseState(Gun gun)
        {
            this.gun = gun;
        }
    }

    public interface State
    {
        void Shot();
        void Fuse();
        void Recharge();
    }

    public class ReadyState : BaseState, State
    {
        public ReadyState(Gun gun) : base(gun) { }

        public void Fuse()
        {
            Console.WriteLine("Поставлен предохранитель");
            gun.SetState(new LockedState(gun));
        }

        public void Recharge()
        {
            Console.WriteLine("Начинаем перезарядку");
            gun.SetState(new ReloadState(gun));
        }

        public void Shot()
        {
            if (gun.Ammunition > 0)
            {
                Console.WriteLine("Выстрел");
                gun.Ammunition--;
            }
        }
    }

    public class LockedState : BaseState, State
    {
        public LockedState(Gun gun) : base(gun) { }

        public void Fuse()
        {
            Console.WriteLine("Снят предохранитель");
            gun.SetState(new ReadyState(gun));
        }

        public void Recharge()
        {
            Console.WriteLine("Автомат на предохранителе.");
        }

        public void Shot()
        {
            Console.WriteLine("Автомат на предохранителе.");
        }
    }

    public class ReloadState : BaseState, State
    {
        public ReloadState(Gun gun) : base(gun) { WaitForEndReload(); }

        public void SetGun(Gun gun)
        {
            this.gun = gun;
        }

        public async Task WaitForEndReload()
        {
            await Task.Delay(10000);
            Console.WriteLine("Перезарядка завершена");
            this.gun.SetState(new ReadyState(gun));
        }

        public void Fuse()
        {
            Console.WriteLine("Ошибка предохранителя! Идёт перезарядка");
        }

        public void Recharge()
        {
            Console.WriteLine("Ошибка перезарядки! Идет предыдущая перезарядка");
        }

        public void Shot()
        {
            Console.WriteLine("Ошибка выстрела! Идет перезарядка");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var gun = new Gun(20);

            gun.Shot();
            gun.Fuse();
            gun.Shot();
            gun.Fuse();
            gun.Shot();
            gun.Recharge();
            gun.Shot();
            Thread.Sleep(10000);
            gun.Shot();

            Console.ReadKey();
        }
    }
}
