﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer__Наблюдатель____Повед
{
    public interface Shop
    {
        void Subscribe(Buyer buyer);
        void Unsubscribe(Buyer buyer);
        void Notify();
    }
    
    public interface Buyer
    {
        void Notify(string message);
    }

    public class ComputerShop : Shop
    {
        List<Buyer> Buyers { get; set; } = new List<Buyer>();

        public void Notify()
        {
            Buyers.ForEach(x => x.Notify("My test message"));
        }

        public void Subscribe(Buyer buyer)
        {
            Buyers.Add(buyer);
        }

        public void Unsubscribe(Buyer buyer)
        {
            Buyers.Remove(buyer);
        }
    }

    public class HTPCompanies : Buyer
    {
        public void Notify(string message)
        {
            Console.WriteLine("HTPCompanies get message: " + message);
        }
    }

    public class SchoolCompanies : Buyer
    {
        public void Notify(string message)
        {
            Console.WriteLine("SchoolCompanies get message: " + message);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new ComputerShop();

            Buyer buyer1 = new HTPCompanies();
            Buyer buyer2 = new SchoolCompanies();

            Console.WriteLine("------------");

            shop.Subscribe(buyer1);
            shop.Subscribe(buyer2);
            shop.Notify();

            Console.WriteLine("------------");

            shop.Unsubscribe(buyer1);
            shop.Notify();

            Console.WriteLine("------------");

            Console.ReadKey();
        }
    }
}
