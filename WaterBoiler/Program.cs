﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterBoiler
{
    public enum WaterConsistency
    {
        Ice,
        Liquid,
        Steam
    }

    public interface State
    {
        void Heart();
        void Cold();
    }

    public class LiquidState : State
    {
        Water water;
        public LiquidState(Water water)
        {
            this.water = water;
            this.water.WaterConsistency = WaterConsistency.Liquid;
        }

        public void Cold()
        {
            Console.WriteLine("Замараживаем воду!");
            water.State = new IceState(water);
        }

        public void Heart()
        {
            Console.WriteLine("Нагреваем воду!");
            water.State = new SteamState(water);
        }
    }

    public class SteamState : State
    {
        Water water;
        public SteamState(Water water)
        {
            this.water = water;
            water.WaterConsistency = WaterConsistency.Steam;
        }

        public void Cold()
        {
            Console.WriteLine("Превращаем в жидкое состояние!");
            water.State = new LiquidState(water);
        }

        public void Heart()
        {
            Console.WriteLine("Продолжаем нагрев!");
        }
    }

    public class IceState : State
    {
        Water water;
        public IceState(Water water)
        {
            this.water = water;
            water.WaterConsistency = WaterConsistency.Ice;
        }

        public void Cold()
        {
            Console.WriteLine("Продолжаем охлаждать лед!");
        }

        public void Heart()
        {
            Console.WriteLine("Превращаем в жидкое состояние!");
            water.State = new LiquidState(water);
        }
    }

    public class Water
    {
        public WaterConsistency WaterConsistency { get; set; }
        public State State { get; set; }

        public Water()
        {
            WaterConsistency = WaterConsistency.Liquid;
            State = new LiquidState(this);
        }

        public void Heart()
        {
            State.Heart();
        }

        public void Cold()
        {
            State.Cold();
        }
    }

    

    class Program
    {
        static void Main(string[] args)
        {
            Water water = new Water();
            Console.WriteLine(water.WaterConsistency);
            water.Heart();
            Console.WriteLine(water.WaterConsistency);
            water.Heart();
            Console.WriteLine(water.WaterConsistency);
            water.Cold();
            Console.WriteLine(water.WaterConsistency);
            water.Cold();
            Console.WriteLine(water.WaterConsistency);
            water.Cold();

            Console.ReadKey();
        }
    }
}
