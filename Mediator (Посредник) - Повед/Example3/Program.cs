﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example3
{
    public abstract class Mediator
    {
        public abstract void Send(bool type);
    }

    public class СoncreteMediator : Mediator
    {
        public PhoneStation1 station1 { get; set; }
        public PhoneStation2 station2 { get; set; }

        public override void Send(bool type)
        {
            if (type == false)
            {
                station1.Enabled = false;
                station2.Enabled = false;
            }
            if (type == true)
            {
                station1.Enabled = true;
                station2.Enabled = true;
            }
        }
    }

    public class PhoneStation
    {
        public bool Enabled { get; set; }
        public string PhoneName { get; set; }

        public delegate void Handler(bool type);
        public event Handler CallEvent;

        public PhoneStation()
        {
            this.Enabled = true;
        }

        public void InvokeCall(string phone)
        {
            if (Enabled)
            {
                CallEvent(false);
                Console.WriteLine("Phone " + PhoneName + "  invoke!");
            }
            else
            {
                Console.WriteLine("Phone " + PhoneName + " not invoke!");
            }
        }

        public void FinishCall()
        {
            CallEvent(true);
            Console.WriteLine("All phones enabled!");
        }
    }

    public class PhoneStation1 : PhoneStation
    {
        public PhoneStation1()
        {

        }
    }

    public class PhoneStation2 : PhoneStation
    {
        public PhoneStation2()
        {

        }
    }

    
    

    class Program
    {
        static void Main(string[] args)
        {
            PhoneStation1 phoneStation1 = new PhoneStation1();
            PhoneStation2 phoneStation2 = new PhoneStation2();

            СoncreteMediator mediator = new СoncreteMediator();
            mediator.station1 = phoneStation1;
            mediator.station2 = phoneStation2;
            phoneStation1.CallEvent += mediator.Send;
            phoneStation2.CallEvent += mediator.Send;

            phoneStation1.InvokeCall("");
            phoneStation2.InvokeCall("");
            phoneStation1.FinishCall();
            phoneStation2.InvokeCall("");

            Console.ReadKey();
        }
    }
}
