﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example2
{
    public abstract class Component
    {
        Mediator meditor;
            
        public Component(Mediator mediator)
        {
            this.meditor = mediator;
        }

        public virtual void Event(string text)
        {
            meditor.notifi(this, text);
        }

        public virtual void Send(string message)
        {
            Console.WriteLine("Сообщение: " + message);
        }
    }

    public class Button : Component
    {
        public Button(Mediator mediator) : base(mediator)
        { }

        public override void Send(string message)
        {
            Console.WriteLine("Сообщение от кнопки: " + message);
        }
    }

    public class CheckBox : Component
    {
        public CheckBox(Mediator mediator) : base(mediator)
        {}

        public override void Send(string message)
        {
            Console.WriteLine("Сообщение от чек бокса: " + message);
        }
    }

    public interface Mediator
    {
        void notifi(Component component, string message);
    }

    public class AuthDialog : Mediator
    {
        public Component button;
        public Component checkBox;
        

        public void notifi(Component component, string message)
        {
            if (component == button)
                checkBox.Send("Нажата кнопка");
            if (component == checkBox)
                button.Send("Нажат чек бох");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            AuthDialog authDialog = new AuthDialog();

            Button button = new Button(authDialog);
            CheckBox checkBox = new CheckBox(authDialog);

            authDialog.button = button;
            authDialog.checkBox = checkBox;

            button.Event("a");
            checkBox.Event("b");

            Console.ReadKey();
        }
    }
}
