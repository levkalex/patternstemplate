﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Command__Команда____Повед
{
    public interface ICommand
    {
        void Execute();
    }

    class EnableTVCommand : ICommand
    {
        TV _tv;
        public EnableTVCommand(TV tv)
        {
            _tv = tv;
        }
        public void Execute()
        {
            if (!_tv.Enabled)
            {
                _tv.EnableTV();
            }
            else
            {
                _tv.DisableTV();
            }
        }
    }

    // Invoker - инициатор
    class Pult
    {
        ICommand _command;
        public Pult(ICommand command)
        {
            _command = command;
        }

        public void PressButton()
        {
            _command.Execute();
        }
    }

    //Receiver
    public class TV
    {
        public bool Enabled { get; set; }

       public void EnableTV()
        {
            Enabled = true;
            Console.WriteLine("Включаем телевизор");
        }

        public void DisableTV()
        {
            Enabled = false;
            Console.WriteLine("Выключаем телевизор");
        }
    }

    public class MicroWawe
    {
        public void StartCooking()
        {
            Console.WriteLine("Микроволновка запущена");
        }

        public void StopCooking()
        {
            Console.WriteLine("Микроволновка остановлена");
        }
    }

    public class MicroCommand : ICommand
    {
        MicroWawe _microWawe;
        public MicroCommand(MicroWawe microWawe)
        {
            _microWawe = microWawe;
        }

        public void Execute()
        {
            _microWawe.StartCooking();
            Thread.Sleep(1000);
            _microWawe.StopCooking();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TV tv = new TV();
            ICommand command = new EnableTVCommand(tv);
            Pult pult = new Pult(command);

            MicroWawe microWawe = new MicroWawe();
            ICommand microCommand = new MicroCommand(microWawe);
            Pult pultMicroWawe = new Pult(microCommand);

            pult.PressButton();
            pult.PressButton();

            pultMicroWawe.PressButton();

            Console.ReadKey();
        }
    }
}
